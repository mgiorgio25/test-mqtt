import os

for root, dirs, files in os.walk("log"):
    sumT = 0
    countT = 0
    maxT = -1
    minT = 100
    for filename in files:
        print(filename)
        with open('log\\'+filename, 'r') as reader:
            count = 0
            sumN = 0
            maxN = -1
            minN = 100
            countConfig = 1

            lines = reader.readlines()
            for line in lines:

                if line == "changes\n":
                    if count != 0:
                        print("\nConfig %i" % countConfig)
                        avg = sumN / count
                        print("Media: %.2f"% (avg))
                        print("Max %i"% maxN)
                        print("Min %i"% minN)
                        print("Count %i"% count)
                    sumN = 0
                    count = 0
                    maxN = -1
                    minN = 100
                    countConfig += 1
                else:
                    number = abs(int(line))
                    sumN += number
                    count += 1

                    if number > maxN:
                        maxN = number

                    if number < minN:
                        minN = number

            sumT += sumN
            countT += count

            if maxN > maxT:
                maxT = maxN

            if minN < minT:
                minT = minN

            # if count != 0:
            #     avg = sumN / count
            #     print("Media: %.2f"% (avg))
            #     print("Max %i"% maxN)
            #     print("Min %i"% minN)
        print('')
    #avgT = sumT / countT
    #print("Media totale: %.2f" % (avgT))
    print("Max totale %i" % maxT)
    print("Min totale %i" % minT)

