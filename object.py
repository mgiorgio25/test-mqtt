import subprocess

import paho.mqtt.client as mqtt
import time
import json


class Object:
    client = mqtt.Client(client_id="test11", clean_session=False)
    topic = ""
    qos = 0
    sub = False

    def __init__(self, host, port, topic, qos, client_id=""):

        #if client_id != "":
            #self.client = mqtt.Client(client_id=client_id, clean_session=False)
        print(host+" "+str(port))
        self.topic = topic
        self.qos = qos
        self.client.on_connect = self.__on_connect
        self.client.on_subscribe = self.__on_subscribe
        self.client.connect(host, port)
        print("Topic: " + topic)

    def __on_connect(self, client, userdata, flags, rc):
        print("Connected with result code " + str(rc))
        if self.sub:
            client.subscribe(self.topic, self.qos)
            client.subscribe(self.topic.split('/')[0]+"/changes", 2)
            print("subscribe")

    def on_message_transmission_time_calculation(self, msg):
        if msg.payload.decode('ascii') == "changes":
            f = open(self.topic.replace("/", "_") + ".log", "a")
            f.write("changes" + '\n')
            f.close()
        unix_time = subprocess.check_output(["echo $(( $(date '+%s%N') / 1000000))"], shell=True).decode('utf-8').splitlines()[0]
        send_time = json.loads(msg.payload.decode('ascii'))['time']
        # print("Send time: " + str(send_time))
        # print("Local time: " + str(unix_time))
        transmission_time = int(unix_time) - int(send_time)
        # print(" transmission_time'" + transmission_time)
        f = open(self.topic.replace("/", "_")+".log", "a")
        f.write(str(transmission_time)+'\n')
        f.close()
        return transmission_time

    def __on_subscribe(self, client, userdata, mid, granted_qos):
        print("Subscribed "+str(client.topic))

    def send_message(self, message):
        msg = self.client.publish(self.topic, message, self.qos)
        return msg

    def listen(self):
        self.client.loop_forever()

