from object import Object
import time
import random
import sys


class Light_Pub(Object):
    def __init__(self, host, port, topic, qos):
        # Calling constructor of
        # Base class
        Object.__init__(self, host, port, topic, qos)

    def loop(self):
        while True:
            usage = random.randint(1, 15)
            state = random.randint(0, 1)
            msg = '{ "time": ' + str(int(time.time() * 1000)) + ', "state": ' + str(state) + ', ' \
                  '"usage": '+str(usage)+'} '
            Object.send_message(self, msg)

            time.sleep(5)


host = sys.argv[1]
port = int(sys.argv[2])
topic = sys.argv[3]
qos = int(sys.argv[4])
switch = Light_Pub(host=host, port=port, topic=topic, qos=qos)
switch.loop()
