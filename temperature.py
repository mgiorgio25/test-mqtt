from object import Object
import random
import time
import sys


class Temperature(Object):
    def __init__(self, host, port, topic, qos):
        # Calling constructor of
        # Base class
        Object.__init__(self, host=host, port=port, topic=topic, qos=qos)

    def loop(self):
        while True:
            msg = '{ "time": '+str(int(time.time() * 1000))+', "temperature": '+str(random.randint(17, 21))+'}'
            msg_sent = Object.send_message(self, msg)
            time.sleep(5)

    def on_publish(self, client, userdata, mid):
        print("Message id "+str(mid))


host = sys.argv[1]
port = int(sys.argv[2])
topic = sys.argv[3]
qos = int(sys.argv[4])
switch = Temperature(host=host, port=port, topic=topic, qos=qos)
switch.loop()
