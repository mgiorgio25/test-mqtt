#!/bin/bash
BASE_TOPIC="home/groudfloor/hallway"
HOST="localhost"
PORT=1883

python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_main_light" 0 &
python3 light_pub.py $HOST $PORT "${BASE_TOPIC}/main_light" 0 &