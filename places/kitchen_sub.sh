#!/bin/bash
BASE_TOPIC="home/groudfloor/kitchen"
HOST="localhost"
PORT=1883

python3 termostat_sub.py $HOST $PORT "${BASE_TOPIC}/heating" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/main_light" 0 &
python3 tv_sub.py $HOST $PORT "${BASE_TOPIC}/tv" 0 &
python3 dish_washer_sub.py $HOST $PORT "${BASE_TOPIC}/dish_washer" 0 &
python3 coffe_macchine_sub.py $HOST $PORT "${BASE_TOPIC}/coffe_macchine" 1 &
python3 hob_sub.py $HOST $PORT "${BASE_TOPIC}/hob" 1 &
python3 water_valve.py $HOST $PORT "${BASE_TOPIC}/water_valve" 1 &
python3 gas_valve.py $HOST $PORT "${BASE_TOPIC}/gas_valve" 1 &
python3 server.py $HOST $PORT "${BASE_TOPIC}" 2 &