#!/bin/bash
BASE_TOPIC="home/groudfloor/livingroom"
HOST="localhost"
PORT=1883

python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_main_light" 1 &
python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_lamp1" 1 &
python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_lamp2" 0 &
python3 temperature.py $HOST $PORT "${BASE_TOPIC}/temperature" 1 &
python3 humidity.py $HOST $PORT "${BASE_TOPIC}/humidity" 1 &
python3 termostat_pub.py $HOST $PORT "${BASE_TOPIC}/heating" 0 &
python3 light_pub.py $HOST $PORT "${BASE_TOPIC}/main_light" 0 &
python3 light_pub.py $HOST $PORT "${BASE_TOPIC}/lamp1" 0 &
python3 tv_pub.py $HOST $PORT "${BASE_TOPIC}/tv" 0 &
python3 home_theatre_pub.py $HOST $PORT "${BASE_TOPIC}/home_theatre" 0 &