#!/bin/bash
BASE_TOPIC="home/firstfloor/childroom1"
HOST="192.168.1.129"
PORT=1883

python3 termostat_sub.py $HOST $PORT "${BASE_TOPIC}/heating" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/main_light" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/light_bedside" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/desk_light" 0 &
python3 server.py $HOST $PORT "${BASE_TOPIC}" 0 &