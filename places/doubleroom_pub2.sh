#!/bin/bash
BASE_TOPIC="home/firstfloor/doubleroom"
HOST="192.168.1.129"
PORT=1883

python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_main_light" 0 &
python3 light_pub.py $HOST $PORT "${BASE_TOPIC}/main_light" 0 &
python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_lamp" 0 &
python3 light_pub.py $HOST $PORT "${BASE_TOPIC}/lamp" 0 &
python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_light_bedside_left" 0 &
python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_light_bedside_right" 0 &