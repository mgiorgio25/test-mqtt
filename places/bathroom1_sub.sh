#!/bin/bash
BASE_TOPIC="home/groudfloor/bathroom"
HOST="localhost"
PORT=1883

python3 termostat_sub.py $HOST $PORT "${BASE_TOPIC}/heating" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/main_light" 0 &
python3 water_valve.py $HOST $PORT "${BASE_TOPIC}/water_valve" 2 &
python3 server.py $HOST $PORT "${BASE_TOPIC}" 0 &