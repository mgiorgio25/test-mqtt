#!/bin/bash
BASE_TOPIC="home/firstfloor/doubleroom"
HOST="192.168.1.129"
PORT=1883

python3 termostat_sub.py $HOST $PORT "${BASE_TOPIC}/heating" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/main_light" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/lamp" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/light_bedside_left" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/light_bedside_right" 0 &
python3 server.py $HOST $PORT "${BASE_TOPIC}" 0 &