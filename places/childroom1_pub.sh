#!/bin/bash
BASE_TOPIC="home/firstfloor/childroom1"
HOST="192.168.1.129"
PORT=1883

python3 temperature.py $HOST $PORT "${BASE_TOPIC}/temperature" 1 &
python3 humidity.py $HOST $PORT "${BASE_TOPIC}/humidity" 1 &
python3 termostat_pub.py $HOST $PORT "${BASE_TOPIC}/heating" 0 &
python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_main_light" 1 &
python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_light_bedside" 0 &
python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_desk_light" 1 &