#!/bin/bash
BASE_TOPIC="home/groudfloor/livingroom"
HOST="localhost"
PORT=1883

python3 tv_sub.py $HOST $PORT "${BASE_TOPIC}/tv" 0 &
python3 home_theatre_sub.py $HOST $PORT "${BASE_TOPIC}/home_theatre" 0 &
python3 termostat_sub.py $HOST $PORT "${BASE_TOPIC}/heating" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/main_light" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/lamp1" 0 &
python3 light_sub.py $HOST $PORT "${BASE_TOPIC}/lamp2" 0 &
python3 server.py $HOST $PORT "${BASE_TOPIC}" 0 &