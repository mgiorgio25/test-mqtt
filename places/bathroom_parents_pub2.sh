#!/bin/bash
BASE_TOPIC="home/firstfloor/bathroom_parents"
HOST="192.168.1.129"
PORT=1883


python3 light_pub.py $HOST $PORT "${BASE_TOPIC}/main_light" 0 &
python3 flood_sensor.py $HOST $PORT "${BASE_TOPIC}/flood_sensor" 0 &