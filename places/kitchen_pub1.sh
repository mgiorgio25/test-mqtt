#!/bin/bash
BASE_TOPIC="home/groudfloor/kitchen"
HOST="192.168.1.129"
PORT=1883

python3 temperature.py $HOST $PORT "${BASE_TOPIC}/temperature" 1 &
python3 humidity.py $HOST $PORT "${BASE_TOPIC}/humidity" 1 &
python3 termostat_pub.py $HOST $PORT "${BASE_TOPIC}/heating" 0 &
python3 switch.py $HOST $PORT "${BASE_TOPIC}/switch_main_light" 0 &
python3 light_pub.py $HOST $PORT "${BASE_TOPIC}/main_light" 0 &
python3 flood_sensor.py $HOST $PORT "${BASE_TOPIC}/flood_sensor" 0 &
python3 gas_sensor.py $HOST $PORT "${BASE_TOPIC}/gas_sensor" 0 &
python3 tv_pub.py $HOST $PORT "${BASE_TOPIC}/tv" 0 &
python3 dish_washer_pub.py $HOST $PORT "${BASE_TOPIC}/dish_washer" 0 &
python3 coffe_macchine_pub.py $HOST $PORT "${BASE_TOPIC}/coffe_macchine" 0 &