import random
import time
import json

from object import Object
import sys


class CentralUnit(Object):
    baseTopic = ""

    def __init__(self, host, port, topic, qos):
        Object.sub = True
        # Calling constructor of
        # Base class
        Object.client.on_message = self.__on_message
        Object.__init__(self, host=host, port=port, topic=topic+"/#", qos=qos)
        #Object.client.subscribe(topic=topic+"/#", qos=qos)
        self.baseTopic = topic

    def __on_message(self, client, userdata, msg):
        print(msg.topic + " " + str(msg.payload) + " QoS  " + str(msg.qos))
        transmission_time = Object.on_message_transmission_time_calculation(self, msg=msg)
        print(transmission_time)

        if msg.topic.split('/')[3].split('_')[0] == "switch":
            tempJson = self.get_base_json().copy()
            tempJson['state'] = json.loads(msg.payload.decode('ascii'))['state']
            switch = msg.topic.split('/')[3].split('_')
            light = ""
            for i in range(1, len(switch)):
                if i < len(switch) - 1:
                    light += switch[i] + '_'
                else:
                    light += switch[i]
            client.publish(self.baseTopic+'/'+light, json.dumps(tempJson), 0)

        if msg.topic.split('/')[3] == "temperature":
            print("one temperature")
            tempJson = self.get_base_json().copy()
            tempJson['temperature'] = json.loads(msg.payload.decode('ascii'))['temperature']
            print("tempjson: "+tempJson)
            client.publish(self.baseTopic+'/heating', json.dumps(tempJson), 0)
            print("msg topic temperature")

        if msg.topic.split('/')[3] == "humidity":
            tempJson = self.get_base_json().copy()
            tempJson['humidity'] = json.loads(msg.payload.decode('ascii'))['humidity']
            client.publish(self.baseTopic+'/heating', json.dumps(tempJson), 0)

        if msg.topic.split('/')[3] == "flood_sensor":
            tempJson = self.get_base_json().copy()
            tempJson['state'] = json.loads(msg.payload.decode('ascii'))['state']
            client.publish(self.baseTopic+'/water_valve', json.dumps(tempJson), 2)

        if msg.topic.split('/')[3] == "gas_sensor":
            tempJson = self.get_base_json().copy()
            tempJson['state'] = json.loads(msg.payload.decode('ascii'))['state']
            client.publish(self.baseTopic+'/gas_valve', json.dumps(tempJson), 2)

    def get_base_json(self):
        return {"time": int(time.time() * 1000)}


host = sys.argv[1]
port = int(sys.argv[2])
topic = sys.argv[3]
qos = int(sys.argv[4])
cu = CentralUnit(host=host, port=port, topic=topic, qos=qos)
cu.listen()
