from object import Object
import sys


class GasValve(Object):
    def __init__(self, host, port, topic, qos):
        Object.sub = True
        # Calling constructor of
        # Base class
        Object.client.on_message = self.__on_message
        Object.__init__(self, host=host, port=port, topic=topic, qos=qos)

    def __on_message(self, client, userdata, msg):
        print(msg.topic + " " + str(msg.payload) + " QoS  " + str(msg.qos))
        transmission_time = Object.on_message_transmission_time_calculation(self, msg=msg)
        print(transmission_time)


host = sys.argv[1]
port = int(sys.argv[2])
topic = sys.argv[3]
qos = int(sys.argv[4])
gas_valve = GasValve(host=host, port=port, topic=topic, qos=qos)
gas_valve.listen()