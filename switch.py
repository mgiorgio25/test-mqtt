from object import Object
import random
import time
import sys


class Switch(Object):
    def __init__(self, host, port, topic, qos):
        # Calling constructor of
        # Base class
        Object.__init__(self, host=host, port=port, topic=topic, qos=qos)

    def loop(self):
        while True:
            msg = '{ "time": '+str(int(time.time() * 1000))+', "state": '+str(random.randint(0, 1))+'}'
            Object.send_message(self, msg)
            time.sleep(5)


host = sys.argv[1]
port = int(sys.argv[2])
topic = sys.argv[3]
qos = int(sys.argv[4])
switch = Switch(host=host, port=port, topic=topic, qos=qos)
switch.loop()
