from object import Object
import time
import random
import sys


class Home_Theatre_Pub(Object):
    def __init__(self, host, port, topic, qos):
        # Calling constructor of
        # Base class
        Object.__init__(self, host, port, topic, qos)

    def loop(self):
        while True:
            input_h = random.randint(1, 10)
            state = random.randint(0, 1)
            volume = random.randint(0, 100)
            msg = '{ "time": ' + str(int(time.time() * 1000)) + ', "state": ' + str(state) + ', ' \
                  '"input": '+str(input_h)+', "volume": '+str(volume)+'} '
            Object.send_message(self, msg)

            time.sleep(5)


host = sys.argv[1]
port = int(sys.argv[2])
topic = sys.argv[3]
qos = int(sys.argv[4])
home_theatre = Home_Theatre_Pub(host=host, port=port, topic=topic, qos=qos)
home_theatre.loop()
