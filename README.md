Per avviare un dispositivo si devono specificare 4 parametri
1. Indrizzo ip del broker
2. Porta del broker
3. Il topic
4. Il QoS

Per esempio
```
python3 switch.py 192.168.1.100 1883 "home/firstfloor/childroom2/switch_main_light" 0
```