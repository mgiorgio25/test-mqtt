from object import Object
import sys
import json


class Receive_Time(Object):
    def __init__(self, host, port, topic, qos):
        Object.sub = True
        # Calling constructor of
        # Base class
        Object.client.on_message = self.__on_message
        Object.__init__(self, host=host, port=port, topic=topic, qos=qos, client_id=topic.replace("/", "_"))

    def __on_message(self, client, userdata, msg):
        transmission_time = Object.on_message_transmission_time_calculation(self, msg=msg)
        num = json.loads(msg.payload.decode('ascii'))['num']
        print(self.topic+" transmission_time'"+str(transmission_time)+"' n'"+str(num)+"' QoS  "+str(msg.qos))


host = sys.argv[1]
port = int(sys.argv[2])
topic = sys.argv[3]
qos = int(sys.argv[4])
receive_time = Receive_Time(host=host, port=port, topic=topic, qos=qos)
receive_time.listen()
