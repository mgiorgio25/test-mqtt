from object import Object
import time
import random
import sys


class Hob_Pub(Object):
    def __init__(self, host, port, topic, qos):
        # Calling constructor of
        # Base class
        Object.__init__(self, host, port, topic, qos)

    def loop(self):
        while True:
            remaining_time = random.randint(1, 1000)
            state = random.randint(0, 1)
            msg = '{ "time": ' + str(int(time.time() * 1000)) + ', "state": ' + str(state) + ', ' \
                  '"remaining_time": '+str(remaining_time)+'} '
            Object.send_message(self, msg)

            time.sleep(5)


host = sys.argv[1]
port = int(sys.argv[2])
topic = sys.argv[3]
qos = int(sys.argv[4])
hob = Hob_Pub(host=host, port=port, topic=topic, qos=qos)
hob.loop()
