#!/bin/bash
BASE_TOPIC="home/time"
HOST="192.168.1.1.129"
PORT=1883

while ! python3 receive_time.py $HOST $PORT "${BASE_TOPIC}" 2 >> test_qos_2.log
do
  sleep 1
  echo "Restarting program..."
done
